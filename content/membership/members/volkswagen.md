---
title: "Volkswagen AG"
date: 2019-03-14T05:10:00-00:00
logo: "/membership/members/logo/VW.jpg"
website: "https://www.volkswagenag.com/"
participation_levels: "Driver"
member_id: 1520
---

The Volkswagen Group comprises ten brands from five European countries: Volkswagen, Volkswagen Commercial Vehicles, ŠKODA, SEAT, CUPRA, Audi, Lamborghini, Bentley, Porsche and Ducati. On our way to becoming a sustainable mobility provider with a role model function in the areas of environment, safety and integrity, Volkswagen Group Innovation rely on a cross-brand and efficient research network. This network connects experts, enables partnership-based cooperation at eye level, creates an innovative working environment and bundles activities in order to increase synergies and efficiencies across the Volkswagen group.
